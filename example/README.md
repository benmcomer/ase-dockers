# Example docker image

This directory contains the setup for a basic alpine docker image.
It allows one to test and demonstrate how images are built and added to the registry via the CI using the `d-in-d` (docker-in-docker) service.

## Basic workflow

This repository maintains Docker images to be used during testing and deployment of ase.
The [CI](../.gitlab-ci.yml) specifies jobs for each image to be built.
Each such job follows more or less the same pattern.
The following segment illustrates the principle:
```yaml
build_example:
  stage: build
  script:
    - IMAGE_NAME=example
    - VERSION=`test -f $IMAGE_NAME/version.sh && source $IMAGE_NAME/version.sh || echo latest`
    - docker build --pull -t $BASE_PATH/$IMAGE_NAME:$VERSION $IMAGE_NAME
    - docker push $BASE_PATH/$IMAGE_NAME:$VERSION
  only:
    changes:
      - example/Dockerfile
      - example/version.sh
```

The job is carried out during the `build` stage of the CI.
The name of the image is specified via the `IMAGE_NAME` variable.
The version can be select by editing the `version.sh` file in the respective subdirectory, here `example/version.sh`.
This file is expected to be a shell script that writes a string to stdout.
As described below this mechanism allows one to maintain different versions of the image in the registry.
The image is build via the `docker build` call.
Internally this relies on using the `d-in-d` (docker-in-docker) service.
Once the build has completed successfully, the image is pushed to the gitlab registry.
The `BASE_PATH` environment variable points to the `ase-dockers` registry.
It is set at the beginning of the CI and is the same for all jobs.

The final block of the job definition (`only: ...`) specifies that this job is only executed when any of the files in the `changes` block change.
Thus when you commit and push changes that are limited to `example/Dockerfile` and `example/version.sh` only the `example` image will be build and pushed.

## Versions

The `version.txt` allows one to pick out specific versions.
The version is read as the first word of the first line of the file, allowing one to annotate in the lines below.
The value of `VERSION` can be made available during building by adding the following two lines to the `Dockerfile`:
```docker
ARG VERSION
ENV VERSION ${VERSION:-}
```
The variable can then be accessed to select e.g., specific git tags.

If the `version.txt` file is missing the [CI](../.gitlab-ci.yml) defaults to `latest`.